$(document).ready(function () {
    if ($('.comunicado-lightbox').length) {
        $('.comunicado-lightbox').simpleLightbox();
    }
    if ($('a[href$=".gif"], a[href$=".jpg"], a[href$=".png"], a[href$=".jpeg"], a[href$=".bmp"]').length) {
        $('a[href$=".gif"], a[href$=".jpg"], a[href$=".png"], a[href$=".jpeg"], a[href$=".bmp"]').simpleLightbox()
    }



    $('.shadow').mouseover(function (e) {
        $(this).addClass('shadow-lg');
    });

    $('.shadow').mouseout(function () {
        $(this).removeClass('shadow-lg')

    });
});