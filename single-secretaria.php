<?php
/**
 * The template for displaying all single posts.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();
?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

    </div>
</div>

<div class="wrapper" id="single-wrapper">

    <div class="container" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="col-12" id="main">

                <?php while (have_posts()) : the_post(); ?>

                    <?php get_template_part('loop-templates/content', 'secretaria'); ?>
                    
                    
                    <?php montebelo_post_nav(); ?>
                    
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    
                    <div class="clearfix"></div>
                    <hr>
                    
                    <p><strong>Telefone:</strong> (54) 3457.2051</p>
                    <p><strong>Endereço:</strong> Rua Sagrada Família, 533, Centro, CEP 95718-000, Monte Belo do Sul/RS</p>

                <div class="clearfix"></div>
                <hr>

                <a href="<?php echo home_url('secretarias') ?>"><small>
                        <i class="fas fa-long-arrow-alt-left fa"></i> Voltar para Secretarias</small>
                </a>

            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->



</div><!-- #single-wrapper -->

<?php get_footer(); ?>