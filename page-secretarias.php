<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod('montebelo_container_type');

?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

    </div>
</div>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">

                <?php echo the_content(); ?>

                <hr>

                <?php $children = get_pages(
                    array(
                        'sort_column' => 'menu_order',
                        'sort_order' => 'ASC',
                        'hierarchical' => 1,
                        'parent' => get_the_ID(),
                        'post_type' => 'page',
                    )
                );
                ?>
                <?php
                foreach ($children as $post) {
                    setup_postdata($post); ?>
                <div class="section-container">
                    <a href="<?php echo the_permalink() ?>">
                        <h3><?php the_title(); ?> <i class="fas fa-long-arrow-alt-right fa"></i></h3>
                    </a>
                    <hr>
                </div>
                <?php }
            wp_reset_postdata();
            wp_reset_query();
            ?>

            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>