<?php get_header(); ?>

<div class="bd-example">
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="<?php echo get_template_directory_uri() ?>/img/home_slider_bg_small.jpg"
                    class="d-block d-sm-none w-100" alt="...">
                <img src="<?php echo get_template_directory_uri() ?>/img/home_slider_bg_large.jpg"
                    class="d-none d-sm-block w-100" alt="...">
                <div class="carousel-caption d-md-block">
                    <div class="container">
                        <p>Prefeitura de</p>
                        <h5>Monte Belo<br />do Sul</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-milk">

    <section class="transparencia bg-01 py-3 py-md-3">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-6">
                    <div class="row d-flex align-items-center">

                        <div class="col-6 col-md-12">
                            <h3 class="text-uppercase">Portal da<br /> Transparência</h3>
                        </div>
                        <div class="col-6 col-md-12">
                            <a href="/portal-da-transparencia/" class="btn btn-simple text-white">acessar <i
                                    class="fas fa-long-arrow-alt-right"></i></a>
                        </div>

                    </div>
                </div>

                <div class="bg-milk rounded col-12 col-md-6 px-4 py-2">

                    <div class="d-flex flex-row justify-content-start">
                        <i class="far fa-hand-point-right fa-1x text-purple float-left mr-3 my-1 ml-0"></i>
                        <div class="alert text-left mb-0 pb-0 text-dark">
                            <strong>Cadastramento de
                                fornecedores</strong> <a
                                href="http://portal.montebelodosul.rs.gov.br/wp-content/uploads/2017/05/AVISO.jpg"
                                class="btn btn-simple text-purple comunicado-lightbox">ver mais <i
                                    class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-start">
                        <i class="far fa-hand-point-right fa-1x text-purple float-left mr-3 my-1 ml-0"></i>
                        <div class="alert text-left mb-0 pb-0 text-dark">
                            <strong>Protocolos Covid-19</strong> <a href="/protocolos-covid-19/"
                                class="btn btn-simple text-purple">ver mais <i
                                    class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-start">
                        <i class="far fa-hand-point-right fa-1x text-purple float-left mr-3 my-1 ml-0"></i>
                        <div class="alert text-left mb-0 pb-0 text-dark">
                            <strong class="">Boletins Covid-19</strong> <a href="/boletins-covid-19/"
                                class="btn btn-simple text-purple">ver mais <i
                                    class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>



    <!-- <div class="alert text-center py-3 d-block d-lg-none">
<strong><i class="fas fa-exclamation-circle"></i> COMUNICADO IMPORTANTE</strong>
<p>O município de Monte Belo do Sul comunica que está promovendo cadastramento de fornecedores de produtos,
serviços e
obras...</p>
<a href="http://portal.montebelodosul.rs.gov.br/wp-content/uploads/2017/05/AVISO.jpg"
class="btn btn-simple text-purple comunicado-lightbox">mais detalhes <i
class="fas fa-long-arrow-alt-right"></i></a>
</div> -->

</div>

<div class="container pt-4 mb-1">
    <a href="https://portal.montebelodosul.rs.gov.br/cadastro-para-transporte-universitario">
        <div class="text-center bg-dark rounded shadow" style="
    background: url(https://portal.montebelodosul.rs.gov.br/wp-content/uploads/2020/07/bus-690508_1920.jpg) center top;
    background-size: cover;
    background-blend-mode: soft-light;
    background-position: center;
    padding: 1em;
    background-color: #542b64 !important;
    ">
            <h4 class="h4 m-0 text-bold cta-title p-1 text-white"><i
                    class="far fa-hand-point-right fa-1x mr-3 ml-0"></i> CADASTRO PARA TRANSPORTE UNIVERSITÁRIO
            </h4>
        </div>
    </a>
</div>

<style>
.cta-title {
    font-family: 'SegoeUI', sans-serif;
    font-weight: 600;
}
</style>

<section class="blog pb-4 pt-2 py-sm-4">
    <div class="container">

        <div class="row">
            <div class="col-12 col-lg-9">

                <h3 class="text-uppercase title mb-4 d-sm-none d-block">Informativos</h3>
                <?php
                $loop = new WP_Query(
                    array(
                    'offset' => 0,
                    'orderby' => 'id',
                    'order' => 'DESC',
                    'posts_per_page' => 1
                    )
                );
                ?>

                <!-- Começa o Loop. -->
                <?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
                <article class="mb-3 mb-md-4 d-flex align-self-start flex-row align-items-start">
                        <?php the_post_thumbnail('medium_large', ['class' => 'img-fluid float-left w-50 mr-3 rounded shadow', 'title' => 'Featured image']); ?>
                    <div>
                        <span class="category"><?php the_category(', '); ?></span>
                        <h3 class="text-uppercase"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <small><?php the_time('d \d\e F \d\e Y') ?></small>
                        <?php the_excerpt('...'); ?>
                        <a class="btn btn-simple text-purple d-none d-md-inline"
                            href="<?php the_permalink() ?>">continue lendo <i
                                class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </article>
                        <?php
                endwhile;
                endif; ?>





                <div class="row justify-content-end align-items-end d-flex">
                    <?php
                    $loop = new WP_Query(
                        array(
                        'offset' => 1,
                        'orderby' => 'id',
                        'order' => 'DESC',
                        'posts_per_page' => 4
                        )
                    );
                    ?>
                    <!-- Começa o Loop. -->
                    <?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>

                    <article class="mb-3 mb-md-4 d-flex align-self-start flex-row align-items-start col-12 col-md-6">
                            <?php the_post_thumbnail('medium_large', ['class' => 'img-fluid float-left w-25 w-lg-40 mr-3 rounded shadow', 'title' => 'Featured image']); ?>
                        <!-- <img src="https://via.placeholder.com/1280x900/333333" alt=""
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                class="img-fluid float-left w-25 w-lg-40 mr-3 rounded shadow-sm"> -->
                        <div>
                            <span class="category"><?php the_category(', '); ?></span>
                            <h4 class="text-uppercase"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
                            <small><?php the_time('d \d\e F \d\e Y') ?></small>
                        </div>
                    </article>

                            <?php
                    endwhile;
else : ?>

                    <p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>


                    <div class="col-12">
                        <hr>
                        <a href="/informativos" class="float-right float-md-left btn btn-simple text-purple">ver tudo <i
                                class="fas fa-long-arrow-alt-right"></i></a>
                    </div>

                </div>
            </div>
            <div class="col-lg-3 d-none d-lg-block">
                <a class="weatherwidget-io w-100 rounded"
                    href="https://forecast7.com/pt/n29d16n51d64/monte-belo-do-sul/"
                    data-label_1="MONTE BELO DO SUL - RS" data-font="Open Sans" data-icons="Climacons Animated"
                    data-days="3" data-theme="weather_one">MONTE BELO DO SUL - RS</a>
                <script>
                ! function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = 'https://weatherwidget.io/js/widget.min.js';
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, 'script', 'weatherwidget-io-js');
                </script>

                <br>

                <a href="/nota-fiscal-gaucha/" target="_blank" class="card shadow p-3">
                    <img src="<?php echo get_template_directory_uri() ?>/img/NOTA-FISCAL-GAUCHA@2x.jpg" alt=""
                        class="img-fluid w-100">
                </a>

                <br>

                <a href="http://www1.tce.rs.gov.br/aplicprod/f?p=50500:4:::NO::F50500_CD_ORGAO:77400&cs=1MermXiq4AMcQQZGTOHM9tphcR5s"
                    target="_blank" class="card shadow p-3">
                    <div class="card-boby p-2">
                        <img src="<?php echo get_theme_file_uri('/img/licitacon.png') ?>" alt=""
                            class="img-fluid w-100">
                    </div>
                </a>

                <br>

                <a href="/ouvidoria-monte-belo-do-sul/" class="card shadow p-3">
                    <img src="/wp-content/uploads/2019/07/78244e02-d7ef-41b3-9e61-65fe249cdf25.png" alt=""
                        class="img-fluid w-100">
                </a>

                <br>

                <a href="/lista-de-inscritos-em-divida-ativa-ref/" class="card shadow p-3">
                    <img src="/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-25-at-09.09.png" alt=""
                        class="img-fluid w-100">
                </a>

            </div>



        </div>
</section>

<a class="weatherwidget-io w-100 d-block d-lg-none" href="https://forecast7.com/pt/n29d16n51d64/monte-belo-do-sul/"
    data-label_1="MONTE BELO DO SUL - RS" data-font="Open Sans" data-icons="Climacons Animated" data-mode="Current"
    data-days="3" data-theme="weather_one">MONTE BELO DO SUL</a>
<script>
! function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (!d.getElementById(id)) {
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://weatherwidget.io/js/widget.min.js';
        fjs.parentNode.insertBefore(js, fjs);
    }
}(document, 'script', 'weatherwidget-io-js');
</script>

<section class="d-block d-lg-none">
    <div class="container">

        <div class="d-flex flex-row justify-content-start align-items-center py-4">
            <!-- <a class="weatherwidget-io w-100" href="https://forecast7.com/pt/n29d16n51d64/monte-belo-do-sul/"
data-label_1="MONTE BELO DO SUL - RS" data-font="Open Sans" data-icons="Climacons Animated"
data-mode="Current" data-days="3" data-theme="weather_one">MONTE BELO DO SUL</a> -->


            <img src="<?php echo get_template_directory_uri() ?>/img/NOTA-FISCAL-GAUCHA@2x.jpg" alt=""
                class="img-fluid w-50 mr-3">
            <div>
                <h1 class="title">NOTA FISCAL GAÚCHA</h1>
                <p>Confira seus pontos acumulados no programa da Nota Fiscal Gaúcha<br>
                    <a href="#" class="btn btn-simple text-purple mt-2">acessar <i
                            class="fas fa-long-arrow-alt-right"></i></a>
                </p>
            </div>

        </div>

    </div>

</section>

<section class="bg-dark mb-md-0 mt-md-3" id="tour-360">
    <div class="container text-white">
        <div class="row">
            <div class="col col-md-5 py-3 p-md-5">
                <h1 class="title">TOUR VIRTUAL EM 360°</h1>
                <p>Contemple a beleza de Monte Belo do Sul em um passeio virtual e encantador em todos os ângulos.</p>
                <a href="/a-cidade/monte-belo-do-sul-em-360o/" class="btn btn-simple text-white">acessar <i
                        class="fas fa-long-arrow-alt-right"></i></a>
            </div>
            <div class="col">
                <!-- <figure style="margin: -35px 0;" class="d-none">
<img src="<?php echo get_template_directory_uri() ?>/img/360.png" alt="" class="img-fluid align-self-center">
</figure> -->
                <div class="w-100 d-none rounded shadow d-md-block"
                    style="height: calc(100% + 70px); margin-top: -35px; margin-bottom: -35px; background-image: url(<?php echo get_template_directory_uri() ?>/img/360.png); background-size: cover; overflow: hidden; border-radius: 4px; background-position: center center;">
                    <!-- <iframe src="http://www.studioi360.com.br/360/temp/montebelo/" width="100%" height="100%"
frameborder="0"></iframe> -->
                </div>
                <div class="w-100 d-block rounded shadow d-md-none"
                    style="height: calc(100% + 10px); margin-top: -5px; margin-bottom: -5px; background-image: url(<?php echo get_template_directory_uri() ?>/img/360.png); background-size: cover; background-position: center center;">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pt-5 bg-03 mb-0 pb-0">
    <div class="container pb-0 mb-0">
        <div class="row  pb-0 mb-0">

            <div class="col pt-0 pb-4 pt-md-2 pb-md-5 pr-md-5">
                <h3 class="text-uppercase title mb-5">Eventos</h3>
                <ul class="events">
                    <?php
                    $loop = new WP_Query(
                        array(
                        'post_type' => 'event',
                        'posts_per_page' => 5
                        )
                    );
                    ?>

                    <?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
                    <li>
                            <?php
                            ?>
                        <div class="icon-calendar mr-3 shadow rounded">
                            <?php rwmb_the_value('event-date', array('format' => 'd')) ?><span
                                class="text-uppercase"><?php echo getMonthPTBR(date('m', strtotime(rwmb_meta('event-date'))), 3); ?></span>
                        </div>
                        <div>
                            <h4>
                                <small><?php rwmb_the_value('event-date') ?></small>
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h4>
                        </div>
                    </li>
                            <?php
                    endwhile;
else : ?>

                    <p>Nenhum evento agendado</p>

<?php endif; ?>
                </ul>
                <a href="/eventos" class="text-left btn btn-simple text-purple w-100">ver todos <i
                        class="fas fa-long-arrow-alt-right"></i></a>
            </div>

            <section class="py-4 p-md-5 col-lg-8 publicacoes-legais">
                <div class="row">


                    <div class="col">

                        <h4 class="text-uppercase title mb-4">Publicações Legais</h4>

                        <ul class="publicacoes border p-2">
                            <?php
                            $loop = new WP_Query(
                                array(
                                'post_type' => 'publicacao_legal',
                                'posts_per_page' => -1,
                                'post_parent' => 0,
                                )
                            );
                            ?>
                            <?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>

                            <li><a class="text-uppercase text-dark btn btn-simple text-purple text-left"
                                    href="<?php the_permalink() ?>"><i class="fas fa-chevron-right"></i>
                                    <?php the_title(); ?></a></li>

                                    <?php
                            endwhile;
                            endif; ?>
                            <!-- <li><a class="text-uppercase text-dark btn btn-simple text-purple text-left"
href="/licitacoes"><i class="fas fa-chevron-right"></i>
Licitações</a></li> -->
                        </ul>


                        <a href="/publicacoes-legais" class="btn btn-simple text-purple text-left w-100">ver tudo <i
                                class="fas fa-long-arrow-alt-right"></i></a>

                    </div>
                    <div class="col">

                        <strong class="text-uppercase page-title">Últimas atualizações:</strong>

                        <hr>

                        <?php
                        // Query Arguments
                        $lastupdated_args = array(
                            'post_type' => array('publicacao_legal', 'licitacao'),
                            'orderby' => 'modified',
                            'ignore_sticky_posts' => '1',
                            'posts_per_page' => 9
                        );

                        //Loop to display 5 recently updated posts
                        $lastupdated_loop = new WP_Query($lastupdated_args);
                        $counter = 1;
                        $string = '<ul>';
                        while ($lastupdated_loop->have_posts() && $counter < 25) : $lastupdated_loop->the_post();
                            $string .= '<li><a href="' . get_permalink($lastupdated_loop->post->ID) . '"> ' . get_the_title($lastupdated_loop->post->ID) . '</a></li>';
                            $counter++;
                        endwhile;
                        $string .= '</ul>';
                        echo $string;
                        wp_reset_postdata();
                        ?>

                        <!-- <small style="font-size: 0.5em;"><i>' . human_time_diff(get_the_time('U'), current_time('timestamp')) . ' atràs</i></small> -->
                        <!-- <small style="font-size: 0.5em;"><i>(' . get_the_modified_date() . ')</i></small> -->

                    </div>
                </div>
            </section>

        </div>
    </div>
</section>


<div class="telefone-uteis alert text-center text-dark py-4 col-12 col-md-6 d-block d-md-none m-0">
    <strong><i class="fas fa-phone"></i> TELEFONES ÚTEIS</strong>
    <p class="my-3">Farmácias, Posto de saúde, Hotéis e outros.<br>Confira lista com os principais números da
        região.</p>
    <a href="#" class="btn btn-simple text-purple">ver telefones <i class="fas fa-long-arrow-alt-right"></i></a>
</div>


<?php get_footer();