<?php get_header(); ?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

        <!-- <figure class="w-100">
<img src="https://source.unsplash.com/random/1600x400" alt="" class="img-fluid">
</figure> -->

    </div>
</div>


<div class="container py-5">

    <div class="row">

        <ul class="events col">
            <?php
            $loop = new WP_Query(array(
                'post_type' => 'event',
                'posts_per_page' => -1,
                'orderby'   => 'meta_value_num',
                'meta_key'  => 'event-date',
                'order'     => 'ASC',
            ));
            ?>

            <?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
            <li>
                <?php
                        ?>
                <div class="icon-calendar mr-3 shadow rounded">
                    <?php rwmb_the_value('event-date', array('format' => 'd')) ?><span
                        class="text-uppercase"><?php echo strftime('%B', rwmb_meta('event-date')) ?></span>
                </div>
                <div>
                    <h4>
                        <small><?php echo strftime('%A', rwmb_meta('event-date')) ?></small>
                        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                    </h4>
                </div>
            </li>
            <?php
            endwhile;
        else : ?>

            <p>Nenhum evento agendado</p>

            <?php endif; ?>
        </ul>

    </div>
</div>
<?php get_footer();