<?php
/**
 * The template for displaying all single posts.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();
?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

    </div>
</div>

<div class="wrapper" id="single-wrapper">

    <div class="container">
        <?php if ($post->post_parent) { ?>
        <a href="<?php echo get_permalink($post->post_parent); ?>"><small>
                <i class="fas fa-long-arrow-alt-left fa"></i> Voltar para
                <?php echo get_the_title($post->post_parent); ?></small>
        </a>
        <?php } else {
        ?>
        <a href="/publicacoes-legais"><small>
                <i class="fas fa-long-arrow-alt-left fa"></i> Voltar para Publicações Legais</small>
        </a>
        <?php
    } ?>
    </div>

    <div class="container" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="col-12" id="main">

                <?php while (have_posts()) : the_post(); ?>

                <?php get_template_part('loop-templates/content', 'publicacao_legal'); ?>

                <?php montebelo_post_nav(); ?>

                <?php
                    // If comments are open or we have at least one comment, load up the comment template.
                    if (comments_open() || get_comments_number()) :
                        comments_template();
                    endif;
                    ?>

                <?php endwhile;
            ?>

            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #single-wrapper -->

<?php get_footer(); ?>