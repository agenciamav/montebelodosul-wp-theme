<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod('montebelo_container_type');

?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

    </div>
</div>

<div class="wrapper" id="archive-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">
                <?php
                $args = array(
                    'post_type' => 'post',
                    // 'posts_per_page' => 3,
                    'paged' => (get_query_var('paged') ? get_query_var('paged') : 1),
                );
                query_posts($args);
                // $query = new WP_Query(array(
                //     'post_type' => 'post',
                //     // 'posts_per_page' => 3,
                // ));
                /* Start the Loop */
                ?>

                <?php if (have_posts()) : ?>

                <?php /* Start the Loop */ ?>

                <?php
                    while (have_posts()) :
                        the_post();

                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part('loop-templates/content', get_post_format());
                        ?>
                <br>

                <?php endwhile; ?>

                <?php else : ?>

                <?php get_template_part('loop-templates/content', 'none'); ?>



                <?php endif; ?>
                <?php wp_reset_postdata(); ?>

            </main><!-- #main -->

            <!-- The pagination component -->
            <?php montebelo_pagination(); ?>

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div> <!-- .row -->

    </div><!-- #content -->

</div><!-- #archive-wrapper -->

<?php get_footer(); ?>