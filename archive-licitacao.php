<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod('montebelo_container_type');
?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">
        <?php get_template_part('inc/titlearea'); ?>
    </div>
</div>


<div class="container bg-secondary pt-2 text-white">
    <?php get_template_part('searchform-licitacao'); ?>
</div>


<div class="wrapper" id="archive-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">


                <?php
                global $query_string;
                $licitacoes = new WP_Query($query_string . "&orderby=meta_value&meta_key=licitacao-publicacao&order=ASC");
                // $licitacoes = new WP_Query($query_string . "&orderby=date&order=DESC");
                if ($licitacoes->have_posts()) :
                    ?>

                <header class="page-header">
                    <?php
                        the_archive_title('<h1 class="page-title">', '</h1>');
                        // the_archive_description('<div class="taxonomy-description">', '</div>');
                        ?>
                </header><!-- .page-header -->

                <hr>
                <?php /* Start the Loop */ ?>
                <?php while ($licitacoes->have_posts()) : $licitacoes->the_post(); ?>
                <?php
                        get_template_part('loop-templates/content-licitacao', 'search');
                        ?>

                <?php endwhile; ?>

                <?php else : ?>

                <?php get_template_part('loop-templates/content', 'none'); ?>

                <?php endif; ?>
                <?php wp_reset_postdata(); ?>

            </main><!-- #main -->

            <!-- The pagination component -->
            <?php montebelo_pagination(); ?>

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div> <!-- .row -->

    </div><!-- #content -->

</div><!-- #archive-wrapper -->

<?php get_footer(); ?>