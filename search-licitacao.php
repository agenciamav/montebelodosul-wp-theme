<?php
/**
 * The template for displaying search results pages.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod('montebelo_container_type');

?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea');?>

    </div>
</div>

<div class="container bg-secondary pt-2 text-white">
    <?php get_template_part('searchform-licitacao');?>
</div>

<div class="wrapper" id="search-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check and opens the primary div -->
            <?php get_template_part('global-templates/left-sidebar-check');?>

            <?php
                if (isset($GLOBALS['searchArgs'])) {

                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $posts_per_page = (get_query_var('posts_per_page')) ? get_query_var('posts_per_page') : 5;
                    // $posts_per_page = 3;
                   
                    $vars = array(
                        'paged' => $paged,
                        'posts_per_page' => $posts_per_page
                    );
                    $query_args = array_merge($GLOBALS['searchArgs'], $vars);

                    $search_query = new WP_Query($query_args);
                    if ($search_query->have_posts()) {
                        ?>

                            <main class="site-main" id="main">
                                <?php
                                if ($search_query->found_posts > 1000) {?>
                                    <div class="alert alert-danger p-3 text-center">
                                        <h3><i class="fa fa-warning"></i> Sua pesquisa retorna muitos resultados</h3>
                                        <p>Tente ser mais específico. Utilize os filtros de Modalidade ou Exercício acima</p>
                                    </div>
                                <?php
                                };
                        while ($search_query->have_posts()) {
                            $search_query->the_post();
                            get_template_part('loop-templates/content-licitacao', 'search');
                        };
                        montebelo_pagination();
                    };
                    wp_reset_postdata(); ?>

            </main><!-- #main -->

            <?php
                } else {
                    get_template_part('loop-templates/content', 'none');
                };?>


            <!-- Do the right sidebar check -->
            <?php // get_template_part('global-templates/right-sidebar-check');?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #search-wrapper -->

<?php get_footer();?>