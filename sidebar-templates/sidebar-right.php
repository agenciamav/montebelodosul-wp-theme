<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!is_active_sidebar('right-sidebar')) {
    return;
}

// when both sidebars turned on reduce col size to 3 from 4.
$sidebar_pos = get_theme_mod('montebelo_sidebar_position');
?>

<?php if ('both' === $sidebar_pos) { ?>
<div class="col-md-3 widget-area" id="right-sidebar" role="complementary">
    <?php } else { ?>
    <div class="col-md-4 widget-area" id="right-sidebar" role="complementary">
        <?php }; ?>
        <?php
        $obj = get_queried_object();

        // print_r($obj);

        $types = array('licitacao');
        $taxonomies = array('exercicio', 'modalidade');

        if (in_array(@$obj->name, $types) || in_array(@$obj->post_type, $types) || in_array(@$obj->taxonomy, $taxonomies)) {
            dynamic_sidebar('right-sidebar-licitacao');
        } else {
            dynamic_sidebar('right-sidebar');
        }
        ?>

        <!-- <a href="/licitacon">
            <img src="<?php echo get_theme_file_uri('/img/licitacon.png') ?>" alt="" class="img-fluid w-100">
        </a> -->

    </div>