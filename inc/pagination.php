<?php
/**
 * Pagination layout.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

if (!function_exists('montebelo_pagination')) {

	function montebelo_pagination($args = array(), $class = 'pagination')
	{

		if ($GLOBALS['wp_query']->max_num_pages <= 1) {
			return;
		}

		$args = wp_parse_args(
			$args,
			array(
				'mid_size'           => 2,
				'prev_next'          => true,
				'prev_text'          => __('<i class="fas fa-long-arrow-alt-left"></i>', 'montebelo'),
				'next_text'          => __('<i class="fas fa-long-arrow-alt-right"></i>', 'montebelo'),
				'screen_reader_text' => __('Posts navigation', 'montebelo'),
				'type'               => 'array',
				'current'            => max(1, get_query_var('paged')),
			)
		);

		$links = paginate_links($args);

		?>

<nav aria-label="<?php echo $args['screen_reader_text']; ?>" class="py-3">

    <ul class="pagination mx-auto">

        <?php
				foreach ($links as $key => $link) {
					?>
        <li class="page-item <?php echo strpos($link, 'current') ? 'active' : ''; ?>">
            <?php echo str_replace('page-numbers', 'page-link', $link); ?>
        </li>
        <?php
			}
			?>

    </ul>

</nav>

<?php
}
}