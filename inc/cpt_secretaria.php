<?php

if (!function_exists('cpt_secretaria')) {

    // Register Custom Post Type
    function cpt_secretaria()
    {
        $labels = array(
            'name' => _x('Secretarias', 'Post Type General Name', 'montebelo'),
            'singular_name' => _x('Secretaria', 'Post Type Singular Name', 'montebelo'),
            'menu_name' => __('Secretarias', 'montebelo'),
            'name_admin_bar' => __('Secretarias', 'montebelo'),
            'archives' => __('Secretarias', 'montebelo'),
            'attributes' => __('Atributos', 'montebelo'),
            'parent_item_colon' => __('Item pai', 'montebelo'),
            'all_items' => __('Todas as Secretarias', 'montebelo'),
            'add_new_item' => __('Adicionar nova Secretaria', 'montebelo'),
            'add_new' => __('Adicionar nova', 'montebelo'),
            'new_item' => __('Adicionar secretaria', 'montebelo'),
            'edit_item' => __('Editar secretaria', 'montebelo'),
            'update_item' => __('Atualizar secretaria', 'montebelo'),
            'view_item' => __('Ver secretaria', 'montebelo'),
            'view_items' => __('Exibir Secretarias', 'montebelo'),
            'search_items' => __('Procurar Secretarias', 'montebelo'),
            'not_found' => __('Não encontrado', 'montebelo'),
            'not_found_in_trash' => __('Não encontrado no lixo', 'montebelo'),
            'featured_image' => __('Imagem em destaque', 'montebelo'),
            'set_featured_image' => __('Definir imagem em destaque', 'montebelo'),
            'remove_featured_image' => __('Remover imagem em destaque', 'montebelo'),
            'use_featured_image' => __('Use como imagem em destaque', 'montebelo'),
            'insert_into_item' => __('Inserir na secretaria', 'montebelo'),
            'uploaded_to_this_item' => __('Enviado para esta secretaria', 'montebelo'),
            'items_list' => __('Lista de Secretarias', 'montebelo'),
            'items_list_navigation' => __('Navegação da lista de Secretarias', 'montebelo'),
            'filter_items_list' => __('Lista de Secretarias', 'montebelo'),
        );
        $rewrite = array(
            'slug'                  => 'secretarias',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args = array(
            'label'                 => __('secretaria', 'montebelo'),
            'description'           => __('Secretarias', 'montebelo'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes', 'post-formats'),
            'taxonomies'            => array('type'),
            'hierarchical'          => true,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => 'secretarias',
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
        );
        register_post_type('secretaria', $args);
    }
    add_action('init', 'cpt_secretaria', 0);
}