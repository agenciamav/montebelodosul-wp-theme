<?php

if (!function_exists('cpt_publicacao_legal')) {

    // Register Custom Post Type
    function cpt_publicacao_legal()
    {
        $labels = array(
            'name' => _x('Publicações Legais', 'Post Type General Name', 'montebelo'),
            'singular_name' => _x('Publicação Legal', 'Post Type Singular Name', 'montebelo'),
            'menu_name' => __('Publicações Legais', 'montebelo'),
            'name_admin_bar' => __('Publicações Legais', 'montebelo'),
            'archives' => __('Publicações Legais', 'montebelo'),
            'attributes' => __('Atributos', 'montebelo'),
            'parent_item_colon' => __('Item pai', 'montebelo'),
            'all_items' => __('Todas as Publicações', 'montebelo'),
            'add_new_item' => __('Adicionar nova publicação legal', 'montebelo'),
            'add_new' => __('Adicionar nova', 'montebelo'),
            'new_item' => __('Adicionar publicação', 'montebelo'),
            'edit_item' => __('Editar publicação', 'montebelo'),
            'update_item' => __('Atualizar publicação', 'montebelo'),
            'view_item' => __('Ver publicação', 'montebelo'),
            'view_items' => __('Exibir publicações', 'montebelo'),
            'search_items' => __('Procurar publicações', 'montebelo'),
            'not_found' => __('Não encontrado', 'montebelo'),
            'not_found_in_trash' => __('Não encontrado no lixo', 'montebelo'),
            'featured_image' => __('Imagem em destaque', 'montebelo'),
            'set_featured_image' => __('Definir imagem em destaque', 'montebelo'),
            'remove_featured_image' => __('Remover imagem em destaque', 'montebelo'),
            'use_featured_image' => __('Use como imagem em destaque', 'montebelo'),
            'insert_into_item' => __('Inserir na publicação', 'montebelo'),
            'uploaded_to_this_item' => __('Enviado para esta publicação', 'montebelo'),
            'items_list' => __('Lista de publicações', 'montebelo'),
            'items_list_navigation' => __('Navegação da lista de publicações', 'montebelo'),
            'filter_items_list' => __('Lista de publicações de filtro', 'montebelo'),
        );
        $rewrite = array(
            'slug'                  => 'publicacoes-legais',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => false,
        );
        $args = array(
            'label'                 => __('Publicação', 'montebelo'),
            'description'           => __('Publicações legais', 'montebelo'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes', 'post-formats'),
            'taxonomies'            => array('type'),
            'hierarchical'          => true,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => 'publicacoes-legais',
            // 'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'     => true,
        );
        register_post_type('publicacao_legal', $args);
    }
    add_action('init', 'cpt_publicacao_legal', 0);
}


add_filter('getarchives_where', 'ucc_getarchives_where_filter', 10, 2);
function ucc_getarchives_where_filter($where, $r)
{
    $args = array('public' => true, '_builtin' => false);
    $output = 'names';
    $operator = 'and';
    $post_types = get_post_types($args, $output, $operator);
    $post_types = array_merge($post_types, array('post', 'publicacao_legal', 'page'));
    $post_types = "'" . implode("' , '", $post_types) . "'";
    return str_replace("post_type = 'post'", "post_type IN ( $post_types )", $where);
}



// function exercicio_pl_taxonomy($t)
// {
//     print_r($t);
//     $labels = array(
//         'name'                       => _x('Exercícios', 'Taxonomy General Name', 'text_domain'),
//         'singular_name'              => _x('Exercício', 'Taxonomy Singular Name', 'text_domain'),
//         'menu_name'                  => __('Exercícios', 'text_domain'),
//         'all_items'                  => __('Todas exercícios', 'text_domain'),
//         'parent_item'                => __('Exercício ascendente', 'text_domain'),
//         'parent_item_colon'          => __('Exercício ascendente:', 'text_domain'),
//         'new_item_name'              => __('Nome da nova exercício', 'text_domain'),
//         'add_new_item'               => __('Adicionar nova exercício', 'text_domain'),
//         'edit_item'                  => __('Editar exercício', 'text_domain'),
//         'update_item'                => __('Atualizar exercício', 'text_domain'),
//         'view_item'                  => __('Ver item', 'text_domain'),
//         'separate_items_with_commas' => __('Separe por vírgulas', 'text_domain'),
//         'add_or_remove_items'        => __('Adicinar ou remover exercícios', 'text_domain'),
//         'choose_from_most_used'      => __('Enscolher uma exercícios', 'text_domain'),
//         'popular_items'              => __('Mais usadas', 'text_domain'),
//         'search_items'               => __('Procurar exercícios', 'text_domain'),
//         'not_found'                  => __('Nada encontrado', 'text_domain'),
//         'no_terms'                   => __('Sem exercícios', 'text_domain'),
//         'items_list'                 => __('Lista de exercícios', 'text_domain'),
//         'items_list_navigation'      => __('Navegação da lista de exercícios', 'text_domain'),
//     );
//     $args = array(
//         'labels'                     => $labels,
//         'hierarchical'               => true,
//         'public'                     => true,
//         'show_ui'                    => true,
//         'show_admin_column'          => true,
//         'show_in_nav_menus'          => true,
//         'show_tagcloud'              => true,
//         'show_in_rest'               => true,
//         'rewrite'                    => array(
//             // 'slug'  => 'licitacoes/%exercicio%'
//             'slug'  => 'publicacoes-legais'
//         )
//     );
//     register_taxonomy('exercicio', array('publicacao_legal'), $args);
// }
// add_action('init', 'exercicio_pl_taxonomy', 0);