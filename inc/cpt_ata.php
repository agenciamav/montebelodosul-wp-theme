<?php

if (!function_exists('cpt_ata')) {

    // Register Custom Post Type
    function cpt_ata()
    {
        $labels = array(
            'name' => _x('Atas', 'Post Type General Name', 'montebelo'),
            'singular_name' => _x('Ata', 'Post Type Singular Name', 'montebelo'),
            'menu_name' => __('Atas', 'montebelo'),
            'name_admin_bar' => __('Atas', 'montebelo'),
            'archives' => __('Atas', 'montebelo'),
            'attributes' => __('Atributos', 'montebelo'),
            'parent_item_colon' => __('Item pai', 'montebelo'),
            'all_items' => __('Todas as Atas', 'montebelo'),
            'add_new_item' => __('Adicionar nova Ata', 'montebelo'),
            'add_new' => __('Adicionar nova', 'montebelo'),
            'new_item' => __('Adicionar ata', 'montebelo'),
            'edit_item' => __('Editar ata', 'montebelo'),
            'update_item' => __('Atualizar ata', 'montebelo'),
            'view_item' => __('Ver ata', 'montebelo'),
            'view_items' => __('Exibir Atas', 'montebelo'),
            'search_items' => __('Procurar Atas', 'montebelo'),
            'not_found' => __('Não encontrado', 'montebelo'),
            'not_found_in_trash' => __('Não encontrado no lixo', 'montebelo'),
            'featured_image' => __('Imagem em destaque', 'montebelo'),
            'set_featured_image' => __('Definir imagem em destaque', 'montebelo'),
            'remove_featured_image' => __('Remover imagem em destaque', 'montebelo'),
            'use_featured_image' => __('Use como imagem em destaque', 'montebelo'),
            'insert_into_item' => __('Inserir na ata', 'montebelo'),
            'uploaded_to_this_item' => __('Enviado para esta ata', 'montebelo'),
            'items_list' => __('Lista de Atas', 'montebelo'),
            'items_list_navigation' => __('Navegação da lista de Atas', 'montebelo'),
            'filter_items_list' => __('Lista de Atas de filtro', 'montebelo'),
        );
        $rewrite = array(
            'slug'                  => 'atas',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args = array(
            'label'                 => __('Ata', 'montebelo'),
            'description'           => __('Atas', 'montebelo'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes', 'post-formats'),
            'taxonomies'            => array('type'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => 'atas',
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'     => true,
        );
        register_post_type('ata', $args);
    }
    add_action('init', 'cpt_ata', 0);
}