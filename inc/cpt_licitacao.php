<?php

if (!function_exists('cpt_licitacao')) {

    // Register Custom Post Type
    function cpt_licitacao()
    {
        $labels = array(
            'name' => _x('Licitações', 'Post Type General Name', 'montebelo'),
            'singular_name' => _x('Licitação', 'Post Type Singular Name', 'montebelo'),
            'menu_name' => __('Licitações', 'montebelo'),
            'name_admin_bar' => __('Licitações', 'montebelo'),
            'archives' => __('Licitações', 'montebelo'),
            'attributes' => __('Atributos', 'montebelo'),
            'parent_item_colon' => __('Item pai', 'montebelo'),
            'all_items' => __('Todos os Licitações', 'montebelo'),
            'add_new_item' => __('Adicionar nova Licitação', 'montebelo'),
            'add_new' => __('Adicionar nova', 'montebelo'),
            'new_item' => __('Adicionar Licitação', 'montebelo'),
            'edit_item' => __('Editar Licitação', 'montebelo'),
            'update_item' => __('Atualizar Licitação', 'montebelo'),
            'view_item' => __('Ver Licitação', 'montebelo'),
            'view_items' => __('Exibir Licitações', 'montebelo'),
            'search_items' => __('Procurar Licitações', 'montebelo'),
            'not_found' => __('Não encontrado', 'montebelo'),
            'not_found_in_trash' => __('Não encontrado no lixo', 'montebelo'),
            'featured_image' => __('Imagem em destaque', 'montebelo'),
            'set_featured_image' => __('Definir imagem em destaque', 'montebelo'),
            'remove_featured_image' => __('Remover imagem em destaque', 'montebelo'),
            'use_featured_image' => __('Use como imagem em destaque', 'montebelo'),
            'insert_into_item' => __('Inserir no Licitação', 'montebelo'),
            'uploaded_to_this_item' => __('Enviado para este Licitação', 'montebelo'),
            'items_list' => __('Lista de Licitações', 'montebelo'),
            'items_list_navigation' => __('Navegação da lista de Licitações', 'montebelo'),
            'filter_items_list' => __('Lista de Licitações', 'montebelo'),
        );
        $rewrite = array(
            'slug'                  => 'licitacoes/%exercicio%',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args = array(
            'label'                 => __('Licitação', 'montebelo'),
            'description'           => __('Licitações', 'montebelo'),
            'labels'                => $labels,
            'supports'              => array('revisions', 'editor'),
            'taxonomies'            => array('modalidade', 'exercicio'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => 'licitacoes',
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'post',
            'show_in_rest'          => true,
        );
        register_post_type('licitacao', $args);
    }
    add_action('init', 'cpt_licitacao', 0);
}


/**
 * METABOXES
 */
function licitacaos_meta_box($meta_boxes)
{
    $prefix = 'licitacao-';

    $meta_boxes[] = array(
        'id' => 'licitacao_details',
        'title' => esc_html__('Detalhes da licitacão', 'montebelo'),
        'post_types' => array('licitacao'),
        'context' => 'form_top',
        'priority' => 'default',
        'autosave' => 'true',
        'fields' => array(
            array(
                'id' => $prefix . 'id-original',
                'type' => 'hidden',
                'name' => esc_html__('Número', 'montebelo'),
                'attributes' => array(
                    'disabled'  => false,
                    'required'  => true,
                ),
            ),
            array(
                'id' => $prefix . 'numero',
                'type' => 'text',
                'name' => esc_html__('Número', 'montebelo'),
                'attributes' => array(
                    'disabled'  => false,
                    'required'  => true,
                ),
            ),
            array(
                'id' => $prefix . 'data-inicial',
                'name' => esc_html__('Data Abertura', 'montebelo'),
                'type' => 'date',
                'js_options' => array(
                    'dateFormat'      => 'dd/mm/yy',
                    'showButtonPanel' => false,
                ),
                'timestamp' => true,
            ),
            array(
                'id' => $prefix . 'data-final',
                'name' => esc_html__('Encerramento', 'montebelo'),
                'type' => 'date',
                'js_options' => array(
                    'dateFormat'      => 'dd/mm/yy',
                    'showButtonPanel' => false,
                ),
                'timestamp' => true,
            ),
            array(
                'id' => $prefix . 'orgao',
                'name' => esc_html__('Órgão', 'montebelo'),
                'type' => 'text'
            ),
            // array(
            //     'name'            => 'Situação',
            //     'id'              => $prefix . 'status',
            //     'type'            => 'select',
            //     'options'         => array(
            //         ''       => '-',
            //         'closed' => 'Encerrada',
            //         'open'   => 'Em andamento',
            //         'winner'   => 'Vencedor',
            //     ),
            //     'multiple'        => false,
            //     'select_all_none' => false,
            // ),
            array(
                'name'            => 'Situação',
                'id'              => $prefix . 'situacao',
                'type'            => 'text',
            ),
            array(
                'name'            => 'Publicação',
                'id'              => $prefix . 'publicacao',
                'type'            => 'hidden',
            ),
        ),
    );

    return $meta_boxes;
}
add_filter('rwmb_meta_boxes', 'licitacaos_meta_box');



// Register Custom Taxonomy
function modalidade_taxonomy()
{
    $labels = array(
        'name'                       => _x('Modalidades', 'Taxonomy General Name', 'text_domain'),
        'singular_name'              => _x('Modalidade', 'Taxonomy Singular Name', 'text_domain'),
        'menu_name'                  => __('Modalidades', 'text_domain'),
        'all_items'                  => __('Todas modalidades', 'text_domain'),
        'parent_item'                => __('Modalidade ascendente', 'text_domain'),
        'parent_item_colon'          => __('Modalidade ascendente:', 'text_domain'),
        'new_item_name'              => __('Nome da nova modalidade', 'text_domain'),
        'add_new_item'               => __('Adicionar nova modalidade', 'text_domain'),
        'edit_item'                  => __('Editar modalidade', 'text_domain'),
        'update_item'                => __('Atualizar modalidade', 'text_domain'),
        'view_item'                  => __('Ver item', 'text_domain'),
        'separate_items_with_commas' => __('Separe por vírgulas', 'text_domain'),
        'add_or_remove_items'        => __('Adicinar ou remover modalidades', 'text_domain'),
        'choose_from_most_used'      => __('Enscolher uma modalidades', 'text_domain'),
        'popular_items'              => __('Mais usadas', 'text_domain'),
        'search_items'               => __('Procurar modalidades', 'text_domain'),
        'not_found'                  => __('Nada encontrado', 'text_domain'),
        'no_terms'                   => __('Sem modalidades', 'text_domain'),
        'items_list'                 => __('Lista de modalidades', 'text_domain'),
        'items_list_navigation'      => __('Navegação da lista de modalidades', 'text_domain'),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'show_in_rest'               => true,
        'rewrite'                    => false
    );
    register_taxonomy('modalidade', array('licitacao'), $args);
}
add_action('init', 'modalidade_taxonomy', 0);


function exercicio_taxonomy()
{
    $labels = array(
        'name'                       => _x('Exercícios', 'Taxonomy General Name', 'text_domain'),
        'singular_name'              => _x('Exercício', 'Taxonomy Singular Name', 'text_domain'),
        'menu_name'                  => __('Exercícios', 'text_domain'),
        'all_items'                  => __('Todas exercícios', 'text_domain'),
        'parent_item'                => __('Exercício ascendente', 'text_domain'),
        'parent_item_colon'          => __('Exercício ascendente:', 'text_domain'),
        'new_item_name'              => __('Nome da nova exercício', 'text_domain'),
        'add_new_item'               => __('Adicionar nova exercício', 'text_domain'),
        'edit_item'                  => __('Editar exercício', 'text_domain'),
        'update_item'                => __('Atualizar exercício', 'text_domain'),
        'view_item'                  => __('Ver item', 'text_domain'),
        'separate_items_with_commas' => __('Separe por vírgulas', 'text_domain'),
        'add_or_remove_items'        => __('Adicinar ou remover exercícios', 'text_domain'),
        'choose_from_most_used'      => __('Enscolher uma exercícios', 'text_domain'),
        'popular_items'              => __('Mais usadas', 'text_domain'),
        'search_items'               => __('Procurar exercícios', 'text_domain'),
        'not_found'                  => __('Nada encontrado', 'text_domain'),
        'no_terms'                   => __('Sem exercícios', 'text_domain'),
        'items_list'                 => __('Lista de exercícios', 'text_domain'),
        'items_list_navigation'      => __('Navegação da lista de exercícios', 'text_domain'),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'show_in_rest'               => true,
        'rewrite'                    => array(
            // 'slug'  => 'licitacoes/%exercicio%'
            'slug'  => 'licitacoes'
        )
    );
    register_taxonomy('exercicio', array('licitacao'), $args);
}
add_action('init', 'exercicio_taxonomy', 0);


function change_licitacao_slug_and_title($post_ID, $post)
{
    if ($post->post_type != 'licitacao') {
        return;
    }

    // use title, since $post->post_name might have unique numbers added
    $exercicio = get_terms_by_custom_post_type('licitacao', 'exercicio', $post_ID);

    $numero = rwmb_meta('licitacao-numero', array(), $post_ID);
    // $new_slug = $numero;

    if (!isset($exercicio[0])) {
        $new_title = 'Licitação ' . date('Y') . '/' . $numero;
    } else {
        $new_title = 'Licitação ' . $exercicio[0]->name . '/' . $numero;
    }

    // unhook this function to prevent infinite looping
    remove_action('save_post', 'change_licitacao_slug_and_title', 10, 2);
    // remove_action('edit_post', 'change_licitacao_slug_and_title', 10, 2);

    wp_update_post(array(
        'ID' => $post_ID,
        // 'post_name' => $new_slug,
        'post_title' => $new_title
    ));

    // re-hook this function
    add_action('save_post', 'change_licitacao_slug_and_title', 10, 2);
    // add_action('edit_post', 'change_licitacao_slug_and_title', 10, 2);
}

if (is_admin()) {
    add_action('save_post', 'change_licitacao_slug_and_title', 10, 2);
}
// add_action('edit_post', 'change_licitacao_slug_and_title', 10, 2);


/**
 * Rewrite Rules
 *
 */
function rewrite_rules()
{
    add_rewrite_rule('licitacoes/([^/]+)/page/?([0-9]{1,})/?$', 'index.php?exercicio=$matches[1]&page=$matches[2]', 'top');
}

/**
 * Taonomies Links
 */
function exercicio_link($link, $post)
{
    if ('licitacao' != $post->post_type) {
        return $link;
    }
    $term = get_terms_by_custom_post_type('licitacao', 'exercicio', $post->ID);
    $term = @$term[0];

    $slug = !empty($term) && !is_wp_error($term) ? $term->slug : date('Y');
    $link = str_replace('%exercicio%', $slug, $link);
    return $link;
}

/**
 * Licitações Archive Link
 *
 */
function licitacao_archive_link($link, $post_type)
{
    if ('licitacao' == $post_type) {
        $link = str_replace('/%exercicio%', '', $link);
    }
    return $link;
}

function ea_is_licitacao_library()
{
    return is_post_type_archive('licitacao') || is_tax(array('exercicio', 'modalidade'));
}

function custom_posts_per_page($query)
{
    // if (!is_admin() && $query->is_post_type_archive('licitacao')) {
    // set_query_var('posts_per_page', 2);
    // set_query_var('posts_per_page', -1);
    // }
}

// Actions
add_action('init', 'rewrite_rules');
add_filter('post_type_link', 'exercicio_link', 10, 2);
add_action('pre_get_posts', 'custom_posts_per_page');


add_filter('template_include', 'search_licitacao_template', 99);

function search_licitacao_template($templates)
{
    $queried_object = get_queried_object();
    if (is_search() && is_object($queried_object)) {
        $templates = locate_template(array("search-licitacao.php", $templates), false);
    }

    return $templates;
}


add_filter('wp_unique_post_slug', 'licitacao_custom_slug', 10, 4);
function licitacao_custom_slug($slug, $post_ID, $post_status, $post_type)
{
    if ($post_type == 'licitacao') {
        // return rwmb_meta('licitacao-numero', array(), $post_ID) ? rwmb_meta('licitacao-numero', array(), $post_ID) : $slug;
        return $post_ID;
    }
    return $slug;
}

function append_slug($data)
{
    global $post_ID;

    if ($data['post_type'] == "licitacao") {
        // $data['post_name'] = rwmb_meta('licitacao-numero', array(), $post_ID);
        $data['post_name'] = $post_ID;
    }
    return $data;
}
if (is_admin()) {
    add_filter('wp_insert_post_data', 'append_slug', 10);
}