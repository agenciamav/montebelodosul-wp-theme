<?php

if (!function_exists('cpt_event')) {

    // Register Custom Post Type
    function cpt_event()
    {
        $labels = array(
            'name' => _x('Eventos', 'Post Type General Name', 'montebelo'),
            'singular_name' => _x('Evento', 'Post Type Singular Name', 'montebelo'),
            'menu_name' => __('Eventos', 'montebelo'),
            'name_admin_bar' => __('Eventos', 'montebelo'),
            'archives' => __('Eventos', 'montebelo'),
            'attributes' => __('Atributos', 'montebelo'),
            'parent_item_colon' => __('Item pai', 'montebelo'),
            'all_items' => __('Todos os Eventos', 'montebelo'),
            'add_new_item' => __('Adicionar novo Evento', 'montebelo'),
            'add_new' => __('Adicionar novo', 'montebelo'),
            'new_item' => __('Adicionar Evento', 'montebelo'),
            'edit_item' => __('Editar Evento', 'montebelo'),
            'update_item' => __('Atualizar Evento', 'montebelo'),
            'view_item' => __('Ver Evento', 'montebelo'),
            'view_items' => __('Exibir Eventos', 'montebelo'),
            'search_items' => __('Procurar Eventos', 'montebelo'),
            'not_found' => __('Não encontrado', 'montebelo'),
            'not_found_in_trash' => __('Não encontrado no lixo', 'montebelo'),
            'featured_image' => __('Imagem em destaque', 'montebelo'),
            'set_featured_image' => __('Definir imagem em destaque', 'montebelo'),
            'remove_featured_image' => __('Remover imagem em destaque', 'montebelo'),
            'use_featured_image' => __('Use como imagem em destaque', 'montebelo'),
            'insert_into_item' => __('Inserir no Evento', 'montebelo'),
            'uploaded_to_this_item' => __('Enviado para este Evento', 'montebelo'),
            'items_list' => __('Lista de Eventos', 'montebelo'),
            'items_list_navigation' => __('Navegação da lista de Eventos', 'montebelo'),
            'filter_items_list' => __('Lista de Eventos', 'montebelo'),
        );
        $rewrite = array(
            'slug'                  => 'eventos',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args = array(
            'label'                 => __('Evento', 'montebelo'),
            'description'           => __('Eventos', 'montebelo'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes', 'post-formats'),
            'taxonomies'            => array('type'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => 'eventos',
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'post',
            'show_in_rest'          => true,
        );
        register_post_type('event', $args);
    }
    add_action('init', 'cpt_event', 0);
}


/**
 * METABOXES
 */
function events_meta_box($meta_boxes)
{
    $prefix = 'event-';

    $meta_boxes[] = array(
        'id' => 'event_details',
        'title' => esc_html__('Detalhes do evento', 'montebelo'),
        'post_types' => array('event'),
        // 'context' => 'after_title',
        'priority' => 'default',
        'autosave' => 'true',
        'fields' => array(
            array(
                'id' => $prefix . 'date',
                'type' => 'date',
                'name' => esc_html__('Data', 'montebelo'),
                // Date picker options. See here http://api.jqueryui.com/datepicker
                'js_options' => array(
                    'dateFormat'      => 'dd/mm/yy',
                    'showButtonPanel' => false,
                ),

                // Display inline?
                'inline' => false,

                // Save value as timestamp?
                'timestamp' => true,
            ),
            array(
                'id' => $prefix . 'time',
                'name' => esc_html__('Hora', 'montebelo'),
                'type' => 'time',
                // Save value as timestamp?
                // 'timestamp' => true,
            ),
            array(
                'id' => $prefix . 'local',
                'name' => esc_html__('Local', 'montebelo'),
                'type' => 'text',
                // Save value as timestamp?
                // 'timestamp' => true,
            ),
        ),
    );

    return $meta_boxes;
}
add_filter('rwmb_meta_boxes', 'events_meta_box');