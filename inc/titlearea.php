<div class="title-area">
    <div class="row">
        <div class="col">
            <header class="page-header">
                <h1>
                    <?php echo get_hansel_and_gretel_breadcrumbs(); ?>
                    <?php wp_reset_query(); ?>
                    <?php wp_reset_postdata(); ?>

                    <br>

                    <h1>

                        <?php
                        if (is_search()) {
                            ?>
                        Resultados da busca
                        <?php
                        } elseif (is_archive()) {
                            echo get_the_archive_title();
                        } elseif (is_tax()) {
                            $wp_the_query   = $GLOBALS['wp_the_query'];
                            $queried_object = $wp_the_query->get_queried_object();
                            // Set the variables for this section
                            $term_object        = get_term($queried_object);
                            $taxonomy           = $term_object->taxonomy;
                            $term_id            = @$term_object->term_id;
                            $term_name          = $term_object->name;
                            $term_parent        = $term_object->parent;
                            $taxonomy_object    = get_taxonomy($taxonomy);
                            echo $taxonomy_object->labels->singular_name . ': ' . $term_name;
                        } else {
                            echo strip_tags(get_the_title());
                        }
                    ?>
                    </h1>
                </h1>
            </header>
        </div>

    </div>
</div>