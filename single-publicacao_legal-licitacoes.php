<?php header('Location: /licitacoes'); ?>

<?php get_header(); ?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

        <!-- <figure class="w-100">
<img src="https://source.unsplash.com/random/1600x400" alt="" class="img-fluid">
</figure> -->

    </div>
</div>


<div class="container py-5">

    <div class="row">

        <?php
        $query = new WP_Query(array(
            'post_type' => 'publicacao_legal',
            'posts_per_page' => -1,
            'order' => 'ASC',
            // 'order' => 'DESC',
            'post_parent' => get_the_ID(),
            // 'posts_per_page' => 3
        ));
        /* Start the Loop */
        while ($query->have_posts()) :
            $query->the_post();
            ?>

        <div class="col-6 col-md-4 mb-4">
            <a href="<?php the_permalink() ?>" id="post-<?php the_ID(); ?>"
                <?php post_class(array('card', 'shadow', 'h-100', 'p-4', 'align-items-center')); ?>>

                <div class="d-flex card-body align-items-center">

                    <h2 class="entry-title text-center ">
                        <?php the_title("", ""); ?>
                    </h2>

                    <?php
                        // $children = get_pages(
                        //         array(
                        //             'sort_column' => 'menu_order',
                        //             'sort_order' => 'ASC',
                        //             'hierarchical' => 0,
                        //             'parent' => get_the_ID(),
                        //             'post_type' => 'publicacao_legal',
                        //         )
                        //     );
                        //     if (sizeof($children)) {
                        ?>
                    <!-- <br>
                                                                                    <ul class="list-group list-group-flush mb-0 p-0 w-100">
                                                                                        <?php
                                                                                        // foreach ($children as $post) {
                                                                                        //     setup_postdata($post); 
                                                                                        ?>
                                                                                            <a class="list-group-item" href="<?php the_permalink() ?>">
                                                                                                <?php the_title(); ?> <i class="fas fa-long-arrow-alt-right fa"></i>
                                                                                            </a>
                                                                                        <?php
                                                                                        ?>
                                                                                    </ul>
                                                                                    <?php
                                                                                    ?> -->

                </div>
            </a>
        </div>
        <?php
    endwhile; // End of the loop.
    ?>

    </div>
</div>
<?php get_footer();