<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <title><?php wp_title(); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if (is_singular() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	} ?>

    <meta name="theme-color" content="#5f356f">

    <?php wp_head(); ?>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top shadow pb-1 ">

        <!-- <div class="overlay" id="overlay">
            <nav class="overlay-menu">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Work</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </nav>
        </div> -->

        <div class="container">

            <a class="navbar-brand" href="/">
                <img src="<?php echo get_template_directory_uri() ?>/img/brasao@2x.png" alt="">
                <small>Prefeitura Municipal de</small><br>
                <span class="d-none d-sm-block">Monte Belo<br />do Sul - RS</span>
                <span class="d-block d-sm-none">Monte Belo do Sul</span>
            </a>

            <!-- <a class="navbar-brand alt align-items-center d-flex flex-column mr-auto"
                href="http://visitemontebelo.com.br" target="_new">
                <small>Visite</small>
                <img src="<?php echo get_template_directory_uri() ?>/img/visitemontebelo.svg" alt="" class="img-fluid">
            </a> -->

            <div class="button_container float-right" id="toggle">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </div>
            <!-- 
            <button class="navbar-toggler" type="button" id="toggle" data-toggle="collapse" data-target="#overlay"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="navbar-toggler-icon">
                    <i class="fas fa-bars"></i>
								</span> 
							</button>
						-->

            <div class=" navbar-collapse flex-column justify-content-start align-items-end">
                <div class="overlay" id="overlay">

                    <nav class="overlay-menu">
                        <ul class="navbar-nav navbar-dark text-uppercase mt-1" id="top-menu">
                            <li class="nav-item d-none d-sm-inline-block">
                                <a class="text-white nav-link">Fale com a prefeitura:</a>
                            </li>

                            <li class="nav-item">
                                <a class="text-white nav-link"
                                    href="https://www.facebook.com/PrefeituradeMonteBelodoSul/" target="_blank"><i
                                        class="fab fa-facebook-square"></i></a>
                            </li>
                            <li class="nav-item">
                                <a class="text-white nav-link"
                                    href="https://www.messenger.com/t/PrefeituradeMonteBelodoSul" target="_blank"><i
                                        class="fab fa-facebook-messenger"></i></a>
                            </li>
                            <li class="nav-item">
                                <a class="text-white nav-link" href="https://goo.gl/maps/BtVQTXKGRbtfdGgF8"
                                    target="_blank"><i class="fas fa-map-marked-alt"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="/contato" class="text-white nav-link"><i
                                        class="fas fa-envelope-open-text"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="callto:+555434572051" class="text-white nav-link"><i
                                        class="fas fa-phone-square"></i>
                                    (54) 3457-2051</a>
                            </li>
                        </ul>

                        <ul class="navbar-nav ml-auto text-uppercase mr-0 mt-3" id="main-menu">

                            <li class="nav-item">
                                <a class="nav-link" href="/a-cidade">A Cidade</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="https://visitemontebelo.com.br" target="_new">Turismo</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Secretarias
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow" aria-labelledby="navbarDropdown">

                                    <?php
									// $page = get_page_by_path('secretarias');
									$loopSecretarias = new WP_Query(array(
										'posts_per_page' => -1,
										'sort_column' => 'menu_order',
										'sort_order' => 'ASC',
										'hierarchical' => 1,
                                        'post_type' => 'secretaria',
                                        'post_parent' => 0,
									));
									?>
                                    <?php if ($loopSecretarias->have_posts()) : while ($loopSecretarias->have_posts()) : $loopSecretarias->the_post(); ?>

                                    <a class="dropdown-item" href="<?php the_permalink() ?>"><?php the_title(); ?> <span
                                            class="caret"></span></a>

                                    <?php
									endwhile;
								endif; ?>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="/publicacoes-legais">Publicações Legais</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="/conselhos-municipais">Conselhos Municipais</a>
                            </li>

                        </ul>
                    </nav>
                </div>

            </div>
        </div>
    </nav>


    <style>

    </style>

    <script>
    $(document).ready(function() {
        $('#toggle').click(function() {
            $(this).toggleClass('active');
            $('#overlay').toggleClass('open');
        });
    });
    </script>
    <?php wp_reset_postdata();