<?php
/**
 * Blank content partial template.
 *
 * @package montebelo
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

the_content();
