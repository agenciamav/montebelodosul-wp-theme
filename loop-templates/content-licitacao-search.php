<?php
/**
 * Single post partial template.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<article <?php post_class(array('mb-5')); ?> id="post-<?php the_ID(); ?>">

    <a href="<?php echo the_permalink(); ?>" class="btn btn-link btn-sm float-right text-violet more d-none">Mais
        detalhes <i class="fas fa-long-arrow-alt-right fa"></i></a>

    <?php
    $exercicio = get_terms_by_custom_post_type('licitacao', 'exercicio', get_the_ID());
    $modalidade = get_terms_by_custom_post_type('licitacao', 'modalidade', get_the_ID());
    ?>

    <?php
    the_title(
        sprintf('<h2 class="entry-title mb-3"><a href="%s" rel="bookmark">', esc_url(get_permalink())),
        '</a></h2>'
    );
    ?>

    <div class="entry-meta p-2 bg-milk two-cols">

        <strong>Número: </strong> <?php rwmb_the_value('licitacao-numero') ?> <br>
        <strong>Exercício: </strong> <?php echo @$exercicio[0]->name ?> <br>
        <strong>Modalidade: </strong> <?php echo @$modalidade[0]->name ?> <br>
        <strong>Situação: </strong> <?php rwmb_the_value('licitacao-status') ?> <br>
        <!-- <strong>Data Abertura: </strong> <?php rwmb_the_value('licitacao-data-inicial') ?> <br> -->
        <!-- <strong>Encerramento: </strong> <?php rwmb_the_value('licitacao-data-final') ?> <br> -->
        <strong>Publicado em: </strong> <?php the_date() ?> <br>
        <strong>Atualizado em: </strong> <?php
                                            $u_time = get_the_time('U');
                                            $u_modified_time = get_the_modified_time('U');
                                            if ($u_modified_time >= $u_time + 86400) {
                                                echo __(the_modified_time('j \d\e F \d\e Y'));
                                                // the_modified_time('F jS, Y');
                                                echo " às ";
                                                the_modified_time();
                                            }
                                            ?>

    </div><!-- .entry-meta -->
</article><!-- #post-## -->

<hr>