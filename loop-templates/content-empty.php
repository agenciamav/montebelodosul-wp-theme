<?php
/**
 * Content empty partial template.
 *
 * @package montebelo
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

the_content();
