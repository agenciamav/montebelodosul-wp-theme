<?php
/**
 * Single post partial template.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

    <header class="entry-header ">

        <?php
        // $exercicio = get_terms(array(
        //     'taxonomy' => 'exercicio',
        //     'hide_empty' => false,
        // ));
        // $modalidade = get_terms(array(
        //     'taxonomy' => 'modalidade',
        //     'hide_empty' => false,
        // ));

        $exercicio = get_terms_by_custom_post_type('licitacao', 'exercicio', get_the_ID());
        $modalidade = get_terms_by_custom_post_type('licitacao', 'modalidade', get_the_ID());

        // print_r($exercicio);
        // exit;
        ?>

        <!-- <h1 class="entry-title mb-4"><?php the_title(); ?></h1> -->

        <?php
        $u_time = get_the_time('U');
        $u_modified_time = get_the_modified_time('U');
        if ($u_modified_time >= $u_time + 86400) {
            echo "<p class='text-secondary'><i class='fas fa-exclamation-triangle'></i> Atualizado dia ";
            echo __(the_modified_time('j \d\e F \d\e Y'));
            // the_modified_time('F jS, Y');
            // echo " às ";
            // the_modified_time();
            echo "</p> ";
        }
        ?>

        <div class="entry-meta p-3 bg-milk two-cols rounded shadow-sm">

            <strong>Número: </strong> <?php rwmb_the_value('licitacao-numero') ?> <br>
            <strong>Exercício: </strong> <?php echo @$exercicio[0]->name ?> <br>
            <strong>Modalidade: </strong> <?php echo @$modalidade[0]->name ?> <br>
            <strong>Situação: </strong> <?php rwmb_the_value('licitacao-situacao') ?> <br>
            <strong>Data Abertura: </strong>
            <!-- <pre><?php print_r(rwmb_meta('licitacao-data-inicial')) ?></pre> -->
            <?php echo (int)rwmb_meta('licitacao-data-inicial') ? strftime('%d de %B de %Y', rwmb_meta('licitacao-data-inicial')) : ""; ?>
            <br>
            <strong>Encerramento: </strong>
            <?php echo (int)rwmb_meta('licitacao-data-final') ? strftime('%d de %B de %Y', rwmb_meta('licitacao-data-final')) : ""; ?>
            <br>
            <strong>Publicação: </strong> <?php the_date() ?> <br>

        </div><!-- .entry-meta -->

    </header><!-- .entry-header -->

    <?php
    ?>

    <div class="entry-content pt-4">

        <?php echo the_content(); ?>

        <br>
        <hr>

        <?php
        wp_link_pages(
            array(
                'before' => '<div class="page-links">' . __('Pages:', 'montebelo'),
                'after'  => '</div>',
            )
        );
        ?>

    </div><!-- .entry-content -->

</article><!-- #post-## -->