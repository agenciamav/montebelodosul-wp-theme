<?php
/**
 * Single post partial template.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

    <header class="entry-header ">

        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>

        <div class="entry-meta">

            <div class="icon-calendar mr-3 shadow rounded">
                <?php rwmb_the_value('event-date', array('format' => 'd')) ?><span
                    class="text-uppercase"><?php echo strftime('%B', rwmb_meta('event-date')) ?></span>
            </div>
            <div>
                <strong>Data: </strong> <?php echo strftime('%A, %d de %B de %Y', rwmb_meta('event-date')); ?> <br />
                <strong>Hora: </strong> <?php rwmb_the_value('event-time'); ?> <br />
                <strong>Local: </strong> <?php rwmb_the_value('event-local'); ?> <br />
            </div>

            <br>

            <?php the_content() ?>

        </div><!-- .entry-meta -->

    </header><!-- .entry-header -->

    <?php
    ?>

    <div class="entry-content">

        <?php
        wp_link_pages(
            array(
                'before' => '<div class="page-links">' . __('Pages:', 'montebelo'),
                'after'  => '</div>',
            )
        );
        ?>

    </div><!-- .entry-content -->

</article><!-- #post-## -->