<?php
/**
 * Single post partial template.
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">


    <div class="entry-content">


        <?php echo the_content(); ?>

        <hr>

        <?php $children = get_pages(
            array(
                'sort_column' => 'menu_order',
                'sort_order' => 'ASC',
                'hierarchical' => 1,
                'parent' => get_the_ID(),
                'post_type' => 'publicacao_legal',
            )
        );
        ?>
        <?php
        foreach ($children as $post) {
            setup_postdata($post); ?>
        <div class="section-container">
            <a href="<?php echo the_permalink() ?>">
                <h3><?php the_title(); ?> <i class="fas fa-long-arrow-alt-right fa"></i></h3>
            </a>
            <hr>
        </div>
        <?php }
    wp_reset_postdata();
    ?>

        <hr>

        <?php
        // wp_link_pages(
        //     array(
        //         'before' => '<div class="page-links">' . __('Pages:', 'montebelo'),
        //         'after'  => '</div>',
        //     )
        // );
        ?>

    </div><!-- .entry-content -->

</article><!-- #post-## -->