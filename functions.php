<?php
// require __DIR__ . '/inc/cpt_publicacao_legal.php';
// require __DIR__ . '/inc/cpt_ata.php';

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$montebelo_includes = array(
    'theme-settings',                  // Initialize theme default settings.
    'setup',                           // Theme setup and custom theme supports.
    'widgets',                         // Register widget area.
    // 'enqueue',                         // Enqueue scripts and styles.
    'template-tags',                   // Custom template tags for this theme.
    'pagination',                      // Custom pagination for this theme.
    'hooks',                           // Custom hooks.
    'extras',                          // Custom functions that act independently of the theme templates.
    'customizer',                      // Customizer additions.
    'custom-comments',                 // Custom Comments file.
    // 'jetpack',                         // Load Jetpack compatibility file.
    'class-wp-bootstrap-navwalker',    // Load custom WordPress nav walker.
    // 'woocommerce',                     // Load WooCommerce functions.
    'editor',                          // Load Editor functions.
    'deprecated',                      // Load deprecated functions.

    'cpt_publicacao_legal',
    'cpt_events',
    'cpt_licitacao',
    'cpt_secretaria',
);

foreach ($montebelo_includes as $file) {
    $filepath = locate_template('inc/' . $file . '.php');
    if (!$filepath) {
        trigger_error(sprintf('Error locating /inc/%s for inclusion', $file), E_USER_ERROR);
    }
    require_once $filepath;
}

add_theme_support('post-thumbnails');


require_once locate_template('widgets/conteudo_interno.php');
require_once locate_template('widgets/licitacoes_modalidades.php');
require_once locate_template('widgets/licitacoes_exercicios.php');


/**
 * Snippet Name: Use last modified time for script enqueue version
 * Snippet URL: http://www.wpcustoms.net/snippets/use-last-modified-time-for-script-enqueue-version/
 */
// reference: wp_enqueue_style( $handle, $src, $deps, $ver, $media );
function enqueue_assets()
{
    wp_enqueue_style('bootstrap', get_theme_file_uri('/node_modules/bootstrap/dist/css/bootstrap.min.css'), null, false);
    wp_enqueue_style('google-fonts', '//fonts.googleapis.com/css?family=Open+Sans', null, false);
    wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.8.0/css/all.css', null, false);
    wp_enqueue_style('simplelightbox', get_theme_file_uri('/node_modules/simplelightbox/dist/simplelightbox.min.css'), null, false);
    wp_enqueue_style('main-css', get_theme_file_uri('/style.css'), null, '1.0');

    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', get_theme_file_uri('/node_modules/jquery/dist/jquery.min.js'), array(), '3.3.1', false);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap', get_theme_file_uri('/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'), array(), '4.3.1', true);
    wp_enqueue_script('simplelightbox', get_theme_file_uri('/node_modules/simplelightbox/dist/simple-lightbox.min.js'), array(), null, false);
    wp_enqueue_script('main-scripts', get_theme_file_uri('/js/scripts.js'), array(), false, false);
}
add_action('wp_enqueue_scripts', 'enqueue_assets');

/**
 * Returns information about the current post's discussion, with cache support.
 */
function get_discussion_data()
{
    static $discussion, $post_id;

    $current_post_id = get_the_ID();
    if ($current_post_id === $post_id) {
        return $discussion; /* If we have discussion information for post ID, return cached object */
    } else {
        $post_id = $current_post_id;
    }

    $comments = get_comments(
        array(
            'post_id' => $current_post_id,
            'orderby' => 'comment_date_gmt',
            'order'   => get_option('comment_order', 'asc'), /* Respect comment order from Settings » Discussion. */
            'status'  => 'approve',
            'number'  => 20, /* Only retrieve the last 20 comments, as the end goal is just 6 unique authors */
        )
    );

    $authors = array();
    foreach ($comments as $comment) {
        $authors[] = ((int)$comment->user_id > 0) ? (int)$comment->user_id : $comment->comment_author_email;
    }

    $authors    = array_unique($authors);
    $discussion = (object)array(
        'authors'   => array_slice($authors, 0, 6),           /* Six unique authors commenting on the post. */
        'responses' => get_comments_number($current_post_id), /* Number of responses. */
    );

    return $discussion;
}



// BREADCRUMBS
function get_hansel_and_gretel_breadcrumbs()
{
    // Set variables for later use
    $here_text        = __('');
    $home_link        = home_url('/');
    $home_text        = __('Portal');
    $link_before      = '<span typeof="v:Breadcrumb">';
    $link_after       = '</span>';
    $link_attr        = ' rel="v:url" property="v:title"';
    $link             = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
    // $delimiter        = ' &raquo; ';              // Delimiter between crumbs
    $delimiter        = ' / ';              // Delimiter between crumbs
    $before           = '<span class="current">'; // Tag before the current crumb
    $after            = '</span>';                // Tag after the current crumb
    $page_addon       = '';                       // Adds the page number if the query is paged
    $breadcrumb_trail = '';
    $category_links   = '';

    /**
     * Set our own $wp_the_query variable. Do not use the global variable version due to
     * reliability
     */
    $wp_the_query   = $GLOBALS['wp_the_query'];
    $queried_object = $wp_the_query->get_queried_object();

    // Handle single post requests which includes single pages, posts and attatchments
    if (is_singular()) {
        /**
         * Set our own $post variable. Do not use the global variable version due to
         * reliability. We will set $post_object variable to $GLOBALS['wp_the_query']
         */
        $post_object = sanitize_post($queried_object);

        // Set variables
        $title          = apply_filters('the_title', $post_object->post_title);
        $parent         = $post_object->post_parent;
        $post_type      = $post_object->post_type;
        $post_id        = $post_object->ID;
        $post_link      = $before . $title . $after;
        $parent_string  = '';
        $post_type_link = '';

        if ('post' === $post_type) {
            // Get the post categories
            $categories = get_the_category($post_id);
            if ($categories) {
                // Lets grab the first category
                $category  = $categories[0];

                $category_links = get_category_parents($category, true, $delimiter);
                $category_links = str_replace('<a', $link_before . '<a' . $link_attr, $category_links);
                $category_links = str_replace('</a>', '</a>' . $link_after, $category_links);
            }
        }

        // if (!in_array($post_type, ['post', 'page', 'attachment'])) {
        $post_type_object = get_post_type_object($post_type);
        $archive_link     = esc_url(get_post_type_archive_link($post_type));
        $post_type_link   = sprintf($link, $archive_link, $post_type_object->labels->name);
        // }

        // Get post parents if $parent !== 0
        if (0 !== $parent) {
            $parent_links = [];
            while ($parent) {
                $post_parent = get_post($parent);

                $parent_links[] = sprintf($link, esc_url(get_permalink($post_parent->ID)), get_the_title($post_parent->ID));

                $parent = $post_parent->post_parent;
            }

            $parent_links = array_reverse($parent_links);

            $parent_string = implode($delimiter, $parent_links);
        }

        // Lets build the breadcrumb trail
        if ($parent_string) {
            $breadcrumb_trail = $parent_string . $delimiter . $post_link;
        } else {
            $breadcrumb_trail = $post_link;
        }

        if ($post_type_link) {
            $breadcrumb_trail = $post_type_link . $delimiter . $breadcrumb_trail;
        }

        if ($category_links) {
            $breadcrumb_trail = $category_links . $breadcrumb_trail;
        }
    }

    // Handle archives which includes category-, tag-, taxonomy-, date-, custom post type archives and author archives
    if (is_archive()) {
        if (
            is_category()
            || is_tag()
            || is_tax()
        ) {
            // Set the variables for this section
            $term_object        = get_term($queried_object);
            $taxonomy           = $term_object->taxonomy;
            $term_id            = $term_object->term_id;
            $term_name          = $term_object->name;
            $term_parent        = $term_object->parent;
            $taxonomy_object    = get_taxonomy($taxonomy);
            $current_term_link  = $before . $taxonomy_object->labels->singular_name . ': ' . $term_name . $after;
            $parent_term_string = '';

            $current_post_type_link  = '';

            // echo "<pre>";
            // print_r($term_object);
            // exit;

            if (is_array($taxonomy_object->object_type)) {
                $cpt_obj = get_post_type_object($taxonomy_object->object_type[0]);
                $current_post_type_link  = $cpt_obj->labels->name; // . $delimiter ;
            }

            if (0 !== $term_parent) {
                // Get all the current term ancestors
                $parent_term_links = [];
                while ($term_parent) {
                    $term = get_term($term_parent, $taxonomy);

                    $parent_term_links[] = sprintf($link, esc_url(get_term_link($term)), $term->name);

                    $term_parent = $term->parent;
                }

                $parent_term_links  = array_reverse($parent_term_links);
                $parent_term_string = implode($delimiter, $parent_term_links);
            }

            if ($parent_term_string) {
                $breadcrumb_trail = $parent_term_string . $current_post_type_link; // . $delimiter . $current_term_link;
            } else {
                $breadcrumb_trail = $current_post_type_link; // . $current_term_link;
            }
        } elseif (is_author()) {
            $breadcrumb_trail = __('Author archive for ') .  $before . $queried_object->data->display_name . $after;
        } elseif (is_date()) {
            // Set default variables
            $year     = $wp_the_query->query_vars['year'];
            $monthnum = $wp_the_query->query_vars['monthnum'];
            $day      = $wp_the_query->query_vars['day'];

            // Get the month name if $monthnum has a value
            if ($monthnum) {
                $date_time  = DateTime::createFromFormat('!m', $monthnum);
                $month_name = $date_time->format('F');
            }

            if (is_year()) {
                $breadcrumb_trail = $before . $year . $after;
            } elseif (is_month()) {
                $year_link        = sprintf($link, esc_url(get_year_link($year)), $year);

                $breadcrumb_trail = $year_link . $delimiter . $before . $month_name . $after;
            } elseif (is_day()) {
                $year_link        = sprintf($link, esc_url(get_year_link($year)), $year);
                $month_link       = sprintf($link, esc_url(get_month_link($year, $monthnum)), $month_name);

                $breadcrumb_trail = $year_link . $delimiter . $month_link . $delimiter . $before . $day . $after;
            }
        } elseif (is_post_type_archive()) {
            $post_type        = $wp_the_query->query_vars['post_type'];
            $post_type_object = get_post_type_object($post_type);

            $breadcrumb_trail = $before . $post_type_object->labels->name . $after;
        }
    }

    // Handle the search page
    if (is_search()) {
        $breadcrumb_trail = __('Search query for: ') . $before . get_search_query() . $after;
    }

    // Handle 404's
    if (is_404()) {
        $breadcrumb_trail = $before . __('Erro 404') . $after;
    }

    // Handle paged pages
    if (is_paged()) {
        $current_page = get_query_var('paged') ? get_query_var('paged') : get_query_var('page');
        $page_addon   = $before . sprintf(__(' ( Página %s )'), number_format_i18n($current_page)) . $after;
    }

    $breadcrumb_output_link  = '';
    $breadcrumb_output_link .= '<small>';
    if (
        is_home()
        || is_front_page()
    ) {
        // Do not show breadcrumbs on page one of home and frontpage
        if (is_paged()) {
            $breadcrumb_output_link .= $here_text . $delimiter;
            $breadcrumb_output_link .= '<a href="' . $home_link . '">' . $home_text . '</a>';
            $breadcrumb_output_link .= $page_addon;
        }
    } else {
        $breadcrumb_output_link .= $here_text;
        $breadcrumb_output_link .= '<a href="' . $home_link . '" rel="v:url" property="v:title">' . $home_text . '</a>';
        $breadcrumb_output_link .= $delimiter;
        $breadcrumb_output_link .= $breadcrumb_trail;
        $breadcrumb_output_link .= $page_addon;
    }
    $breadcrumb_output_link .= '</small><!-- .breadcrumbs -->';

    return $breadcrumb_output_link;
}




add_filter('get_the_archive_title', 'modify_archive_title');
function modify_archive_title($title)
{
    if (is_category()) {
        $title = single_cat_title('', false);
    }
    return $title;
}


/**
 *  Allow duplicated slugs
 */
function allow_duplicate_slugs($slug, $post_id, $post_status, $post_type, $post_parent, $original_slug)
{
    global $wpdb, $wp_rewrite;

    $pto = get_post_type_object($post_type);

    # If our post type isn't hierarchical, we don't need to worry about it:
    if (!$pto->hierarchical) {
        return $slug;
    }

    # If our slug doesn't end with a number, we don't need to worry about it:
    // if ( !preg_match( '|[0-9]$|', $slug ) )
    // 	return $slug;

    # Most of this code is pulled straight from wp_unique_post_slug(). Just the post type check has changed.

    // $feeds = $wp_rewrite->feeds;
    // if ( ! is_array( $feeds ) )
    // 	$feeds = array();

    // $check_sql = "
    // 	SELECT post_name
    // 	FROM $wpdb->posts
    // 	WHERE post_name = %s
    // 	AND post_type = %s
    // 	AND ID != %d
    // 	AND post_parent = %d
    // 	LIMIT 1
    // ";
    // $post_name_check = $wpdb->get_var( $wpdb->prepare( $check_sql, $original_slug, $post_type, $post_id, $post_parent ) );

    // if ( $post_name_check || in_array( $original_slug, $feeds ) || preg_match( "@^($wp_rewrite->pagination_base)?\d+$@", $original_slug )  || apply_filters( 'wp_unique_post_slug_is_bad_hierarchical_slug', false, $original_slug, $post_type, $post_parent ) ) {
    // 	$suffix = 2;
    // 	do {
    // 		$alt_post_name = substr( $original_slug, 0, 200 - ( strlen( $suffix ) + 1 ) ) . "-$suffix";
    // 		$post_name_check = $wpdb->get_var( $wpdb->prepare( $check_sql, $alt_post_name, $post_type, $post_id, $post_parent ) );
    // 		$suffix++;
    // 	} while ( $post_name_check );
    // 	$slug = $alt_post_name;
    // } else {
    // 	$slug = $original_slug;
    // }

    return $original_slug;
};

add_filter('wp_unique_post_slug', 'allow_duplicate_slugs', 10, 6);



function get_terms_by_custom_post_type($post_type = 'post', $taxonomy, $post_id = null)
{
    // exit;

    // $args = array( 'post_type' => $post_type);
    // $loop = new WP_Query($args);
    $postids = array();
    if ($post_id !== null) {
        array_push($postids, $post_id);
    } else {
        $terms = get_terms(array(
            'taxonomy' => $taxonomy,
            'hide_empty' => false,
            'parent'   => 0,
            'post_type' => $post_type
        ));
        return $terms;
        // build an array of post IDs
        // while ($loop->have_posts()) : $loop->the_post();
        // array_push($postids, get_the_ID());
        // endwhile;
    }

    // exit;
    // // get taxonomy values based on array of IDs
    $regions = wp_get_object_terms($postids, $taxonomy);
    return $regions;
}


function change_archive_titles($title)
{
    if (is_admin()) {
        return $title;
    }

    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
        // } elseif (is_tax()) {
        //     $title = single_term_title('', false);
    }

    return $title;
}
add_filter('get_the_archive_title', 'change_archive_titles');
