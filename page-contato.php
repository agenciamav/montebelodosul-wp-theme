<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod('montebelo_container_type');

?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

    </div>
</div>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <main class="site-main two-cols" id="main">

            <?php the_content(); ?>

        </main><!-- #main -->

    </div><!-- #page-wrapper -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>