<?php
if (is_home()) {
    ?>
<section class="leis">
    <div class="wrapper py-4">

        <div class="container">
            <div class="row">

                <div class="col-md-6 col-12 py-0">
                    <div class="row">
                        <div class="col-6 col-md-12">
                            <h3 class="text-uppercase">Leis Municipais</h3>
                        </div>
                        <div class="col-6 col-md-12">
                            <a href="/leis-municipais" class="btn btn-simple text-white">acessar <i
                                    class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="telefone-uteis col-4 text-center text-dark ml-auto">
                    <div class="d-lg-flex align-items-start pt-2">
                        <i class="far fa-hand-point-right fa-2x text-purple float-left mr-3 mb-3 ml-0"></i>
                        <div class="alert text-left mb-0 pb-0 text-dark">
                            <strong>Telefones Úteis</strong><br>
                            <a href="/telefones-uteis" class="btn btn-simple text-purple">ver telefones <i
                                    class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

    <?php
} else {
    ?>
<section class="leis">
    <div class="wrapper py-4">
        <div class="container align-items-center">
            <div class="row">
                <div class="col-6">
                    <div class="row d-flex align-items-end">

                        <div class="col">
                            <h3 class="text-uppercase">Portal da<br /> Transparência</h3>
                        </div>
                        <div class="col pb-2">
                            <a href="/portal-da-transparencia/" class="btn btn-simple text-white">acessar <i
                                    class="fas fa-long-arrow-alt-right"></i></a>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 d-lg-flex align-items-start pt-0">
                    <i class="far fa-hand-point-right fa-2x text-purple float-left mr-3 mb-3 ml-0"></i>
                    <div class="alert text-left mb-0 pb-0 text-dark">
                        <strong>Cadastramento de
                            fornecedores</strong><br>
                        <a href="http://portal.montebelodosul.rs.gov.br/wp-content/uploads/2017/05/AVISO.jpg"
                            class="btn btn-simple text-purple comunicado-lightbox">ver mais <i
                                class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 d-lg-flex align-items-start pt-0">
                    <i class="far fa-hand-point-right fa-2x text-purple float-left mr-3 mb-3 ml-0"></i>
                    <div class="alert text-left mb-0 pb-0 text-dark">
                        <strong>Protocolos Covid-19</strong><br>
                        <a href="/protocolos-covid-19/" class="btn btn-simple text-purple">ver mais <i
                                class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
</section>

    <?php
}
?>



<footer class="mastfoot mt-0 text-secondary">
    <div class="container pt-4">

        <div class="row text-secondary my-4">

            <div class="col-12 col-lg-3">
                <a class="navbar-brand d-block mx-auto mx-md-0 mb-2" href="#">
                    <img src="<?php echo get_template_directory_uri() ?>/img/brasao@2x.png" alt="" height="40">
                    <small>Prefeitura Municipal de</small><br>
                    Monte Belo do Sul
                </a>

                <hr class="d-block d-lg-none">
            </div>

            <div class="col col-lg-3">

                <p>Rua Sagrada Família, 533<br />
                    Centro. CEP 95718-000<br />
                    Monte Belo do Sul - RS<br />
                    <strong>(54) 3457-2051</strong></p>

                <p class="social mb-3">
                    <a class="text-violet" href="https://www.facebook.com/PrefeituradeMonteBelodoSul/"
                        target="_blank"><i class="fab fa-facebook-square"></i></a>
                    <a class="text-violet" href="https://www.messenger.com/t/PrefeituradeMonteBelodoSul"
                        target="_blank"><i class="fab fa-facebook-messenger"></i></a>
                    <a class="text-violet" href="callto:+555434572051" title="(54) 3457-2051"><i
                            class="fas fa-phone-square"></i></a>
                    <a class="text-violet" href="mailto:contato@montebelodosul.rs.gov.br"
                        title="contato@montebelodosul.rs.gov.br"><i class="fas fa-envelope-open-text"></i></a>
                </p>

                <p><strong>Horário de atendimento:</strong><br />
                    07:30 às 11:30 e<br />
                    13:00 às 17:00.<br />
                    Segunda a Sexta-feira.</p>

            </div>

            <div class="col d-none d-lg-block">
                <ul class="">
                    <?php
                    // $page = get_page_by_path('secretarias');
                    $loopSecretarias = new WP_Query(
                        array(
                        'posts_per_page' => -1,
                        'sort_column' => 'menu_order',
                        'sort_order' => 'ASC',
                        'hierarchical' => 1,
                        'post_type' => 'secretaria',
                        'post_parent' => 0,
                        )
                    );
                    ?>
                    <?php if ($loopSecretarias->have_posts()) : while ($loopSecretarias->have_posts()) : $loopSecretarias->the_post(); ?>

                    <li><a class="text-uppercase" href="<?php the_permalink() ?>">
                            <?php the_title(); ?> <i class="ml-2 fas fa-long-arrow-alt-right"></i></a></li>

                            <?php
                    endwhile;
                    endif; ?>
                </ul>
            </div>

            <div class="col-auto">
                <ul class="">
                    <li><a href="http://visitemontebelo.com.br/" class="">VISITE MONTE BELO <i
                                class="ml-2 fas fa-long-arrow-alt-right"></i></a></li>
                    <li><a class="">MAPA DO SITE <i class="ml-2 fas fa-long-arrow-alt-right"></i></a></li>
                    <li><a href="/eventos" class="">AGENDA DE EVENTOS <i
                                class="ml-2 fas fa-long-arrow-alt-right"></i></a></li>
                    <li><a class="">GALERIA DE FOTOS <i class="ml-2 fas fa-long-arrow-alt-right"></i></a></li>
                    <li><a href="/publicacoes-legais" class="">PUBLICAÇÕES LEGAIS <i
                                class="ml-2 fas fa-long-arrow-alt-right"></i></a></li>
                    <li><a href="/licitacoes" class="">LICITAÇÕES <i class="ml-2 fas fa-long-arrow-alt-right"></i></a>
                    </li>
                    <li><a class="">LEI DE RESPONSABILIDADE FISCAL <i class="ml-2 fas fa-long-arrow-alt-right"></i></a>
                    </li>
                    <li><a href="/informativos" class="">INFORMATIVOS <i
                                class="ml-2 fas fa-long-arrow-alt-right"></i></a></li>
                </ul>
            </div>

        </div>

    </div>
    <div class="container text-center py-md-4">

        <p class="text-secondary copyright mx-auto">Copyright &copy; 2019. Prefeitura Municipal de Monte Belo do Sul.
            Todos os direitos reservados.<br />Ao navegar neste site você concorda com a nossa <a
                href="/politica-de-privacidade">política de privacidade</a>.</p>

        <hr>

        <p class="powered-by-mav py-2">
            <small class="text-secondary">Mantido com <i class="fas fa-heart" title="amor"></i> por</small>

            <a href="#">
                <img src="<?php echo get_template_directory_uri() ?>/img/logo_mav.svg" alt="" class="">
            </a>

            <br>

            <!-- <small><?php echo date("d-m-Y H:i"); ?></small> -->
        </p>

    </div>


</footer>

<?php wp_footer(); ?>

<script id="dsq-count-scr" src="//portalmontebelodosul.disqus.com/count.js" async></script>
<script>
var disqus_config = function() {
    this.language = "pt";
};
</script>

<script type="text/javascript">
window.smartlook || (function(d) {
    var o = smartlook = function() {
            o.api.push(arguments)
        },
        h = d.getElementsByTagName('head')[0];
    var c = d.createElement('script');
    o.api = new Array();
    c.async = true;
    c.type = 'text/javascript';
    c.charset = 'utf-8';
    c.src = 'https://rec.smartlook.com/recorder.js';
    h.appendChild(c);
})(document);
smartlook('init', 'b7624ee7f4c02f3be65b9155ec7f7e2df5d19199');
</script>

</body>

</html>
