<?php
// Adds widget: Lista de Modalidades
class ListaModalidades_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'listamodalidades_widget',
            esc_html__('Lista de Modalidades', 'montebelo')
        );
    }

    private $widget_fields = array(
        array(
            'label' => 'Depth',
            'id' => 'depth_number',
            'default' => '1',
            'type' => 'number',
        ),
    );

    public function widget($args, $instance)
    {
        $terms = get_terms('modalidade');

        echo '<h4 class="widget-title">Modalidades</h4>';
        echo '<ul class="flex-column">';

        $queried_term = @get_queried_object()->term_id;

        foreach ($terms as $term) {
            $active = '';
            if ($queried_term && $queried_term == $term->term_id) {
                $active = 'active';
            }
            echo '<li class="nav-item"><a class="nav-link ' . $active . '" href="' . get_term_link($term) . '"><i class="fas fa-chevron-right d-none"></i> ' . $term->name . '</a></li>';
        }

        echo '</ul>';
    }

    public function field_generator($instance)
    {
        $output = '';
        foreach ($this->widget_fields as $widget_field) {
            $default = '';
            if (isset($widget_field['default'])) {
                $default = $widget_field['default'];
            }
            $widget_value = !empty($instance[$widget_field['id']]) ? $instance[$widget_field['id']] : esc_html__($default, 'montebelo');
            switch ($widget_field['type']) {
                default:
                    $output .= '<p>';
                    $output .= '<label for="' . esc_attr($this->get_field_id($widget_field['id'])) . '">' . esc_attr($widget_field['label'], 'montebelo') . ':</label> ';
                    $output .= '<input class="widefat" id="' . esc_attr($this->get_field_id($widget_field['id'])) . '" name="' . esc_attr($this->get_field_name($widget_field['id'])) . '" type="' . $widget_field['type'] . '" value="' . esc_attr($widget_value) . '">';
                    $output .= '</p>';
            }
        }
        echo $output;
    }

    public function form($instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'montebelo'); ?>
<p>
    <label
        for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'montebelo'); ?></label>
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
        name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
        value="<?php echo esc_attr($title); ?>">
</p>
<?php
    $this->field_generator($instance);
}

public function update($new_instance, $old_instance)
{
    $instance = array();
    $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
    foreach ($this->widget_fields as $widget_field) {
        switch ($widget_field['type']) {
            default:
                $instance[$widget_field['id']] = (!empty($new_instance[$widget_field['id']])) ? strip_tags($new_instance[$widget_field['id']]) : '';
        }
    }
    return $instance;
}
}

function register_listamodalidades_widget()
{
    register_widget('ListaModalidades_Widget');
}
add_action('widgets_init', 'register_listamodalidades_widget');