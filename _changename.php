<?php
require('../../../wp-load.php');

$args = [
    'post_type' => 'licitacao',
];

$licitacoes = new WP_Query($args);

if ($licitacoes->have_posts()) : while ($licitacoes->have_posts()) : $licitacoes->the_post(); ?>
<li>
    <?php
            $post_id = wp_update_post(['ID' => $licitacoes->the_post_ID()], true);
            if (is_wp_error($post_id)) {
                $errors = $post_id->get_error_messages();
                foreach ($errors as $error) {
                    echo $error;
                }
            } else {
                print_r($post_id);
            }
            ?>
</li>
<?php
endwhile;
else : ?>

<p>Nada</p>

<?php endif;