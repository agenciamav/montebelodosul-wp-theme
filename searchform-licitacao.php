<?php
/**
 * The template for displaying search forms
 *
 * @package montebelo
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}


$numero = isset($_GET['numero']) ? $_GET['numero'] : '';
$exercicio = isset($_GET['exercicio']) ? $_GET['exercicio'] : '';
$modalidade = isset($_GET['modalidade']) ? $_GET['modalidade'] : '*';
$objeto = isset($_GET['objeto']) ? $_GET['objeto'] : '';

$searchArgs = [
    'post_type' => 'licitacao',
    'posts_per_page' => -1,
    'tax_query' => array(),
    'meta_query' => array(),
];

if ($numero) {
    $searchArgs['meta_query'][] = array(
        'key' => 'licitacao-numero',
        'value' => $numero,
        'compare' => 'LIKE',
        // 'type' => 'NUMERIC'
    );
    $searchArgs['meta_query']['relation'] = 'AND';
}


if ($exercicio) {
    $term = get_term_by('name', $exercicio, 'exercicio');
    $searchArgs['tax_query'][] = array(
        'taxonomy' => 'exercicio',
        'terms' => $term->term_id
    );
}
if ($modalidade) {
    $searchArgs['tax_query'][] = array(
        'taxonomy' => 'modalidade',
        'field'     => 'slug',
        'terms' => $modalidade,
        'operator'  => 'IN'
    );
}
if ($exercicio && $modalidade) {
    // $searchArgs['tax_query']['relation'] = 'AND'; // Default is AND
}

if ($objeto) {
    $searchArgs['s'] = $objeto;
}

$GLOBALS['searchArgs'] = $searchArgs;
?>

<form method="get" action="<?php echo esc_url(home_url('/licitacoes')); ?>" role="search" id="search-licitacao">

    <!-- <input type="hidden" name="post_type" value="licitacao"> -->
    <input type="hidden" name="s" value="">

    <?php $queried_object = get_queried_object() ?>

    <div class="form-row">
        <div class="form-group col">
            <label for="">Número</label>
            <input type="text" class="form-control" value="<?php echo esc_attr($numero); ?>" name="numero">
        </div>
        <div class="form-group col">
            <label for="inputState">Exercício</label>
            <select class="form-control" name="exercicio">
                <option value="">--Selecione um--</option>
                <?php
                $terms = get_terms('exercicio');

                foreach ($terms as $term) {

                    // The $term is an object, so we don't need to specify the $taxonomy.
                    $term_link = get_term_link($term);

                    // If there was an error, continue to the next term.
                    if (is_wp_error($term_link)) {
                        continue;
                    }

                    // We successfully got a link. Print it out.
                    $selected =  $exercicio != $term->slug ?: 'selected';
                    echo '<option value="' . $term->slug . '" ' .  $selected . '>' . $term->name . '</option>';
                }

                ?>
            </select>
        </div>
        <div class="form-group col">
            <label for="inputState">Modalidade</label>
            <select id="" class="form-control" name="modalidade">

                <option value="">--Selecione um--</option>
                <?php
                $terms = get_terms('modalidade');

                foreach ($terms as $term) {

                    // The $term is an object, so we don't need to specify the $taxonomy.
                    $term_link = get_term_link($term);

                    // If there was an error, continue to the next term.
                    if (is_wp_error($term_link)) {
                        continue;
                    }

                    // We successfully got a link. Print it out.
                    $selectedModalidade =  $modalidade != $term->slug ?: 'selected';
                    echo '<option value="' . $term->slug . '" ' . $selectedModalidade . '>' . $term->name . '</option>';
                }

                ?>

            </select>
        </div>
        <div class="form-group col">
            <label>Objeto</label>
            <input type="text" class="form-control" name="objeto" value="<?php echo esc_attr($objeto); ?>">
        </div>
        <div class="form-group col">
            <label for="">&nbsp;</label>
            <br>
            <a href="#" class="btn btn-primary btn-block"
                onclick="document.getElementById('search-licitacao').submit(); return false;"><i
                    class="fas fa-search"></i>
                Procurar </a>
        </div>
    </div>
</form>