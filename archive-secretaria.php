<?php get_header(); ?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

    </div>
</div>


<div class="container py-5">

    <div class="row">

        <?php
        while (have_posts()) :
            the_post();
            ?>

        <div class="col-4 mb-4">
            <div id="post-<?php the_ID(); ?>" <?php post_class(array('card', 'shadow', 'h-100')); ?>>
                <a href="<?php the_permalink() ?>"
                    class="card-body d-flex justify-content-center align-items-center p-5">

                    <h2 class="entry-title text-center "><?php the_title("", ""); ?></h2>
                </a>

            </div>
        </div>
        <?php
    endwhile; // End of the loop.
    ?>
    </div>
</div>
<?php get_footer();