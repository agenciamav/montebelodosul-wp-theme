<?php get_header(); ?>

<div class="bg-dark d-block w-100 h-50 text-white">
    <div class="container pt-5 pb-2">

        <?php get_template_part('inc/titlearea'); ?>

        <!-- <figure class="w-100">
<img src="https://source.unsplash.com/random/1600x400" alt="" class="img-fluid">
</figure> -->

    </div>
</div>


<div class="container py-5">

    <div class="row">

        <!-- <div class="col-4 mb-4">
            <div class="card shadow publicacao_legal type-publicacao_legal status-publish format-standard hentry">

                <div class="card-body">

                    <h2 class="entry-title text-center"><a href="/licitacoes">Licitações</a></h2>

                </div>
            </div>
        </div> -->

        <?php
        $query = new WP_Query(array(
            'post_type' => 'publicacao_legal',
            'posts_per_page' => -1,
            'order' => 'ASC',
            // 'order' => 'DESC',
            'post_parent' => 0,
            // 'posts_per_page' => 3
        ));
        /* Start the Loop */
        while ($query->have_posts()) :
            $query->the_post();
            ?>

        <div class="col-4 mb-4">
            <div id="post-<?php the_ID(); ?>" <?php post_class(array('card', 'shadow')); ?>>

                <div class="card-body">

                    <h2 class="entry-title text-center "><a
                            href="<?php the_permalink() ?>"><?php the_title("", ""); ?></a>
                    </h2>
                    <br>
                    <?php $children = get_pages(
                            array(
                                'sort_column' => 'menu_order',
                                'sort_order' => 'ASC',
                                'hierarchical' => 0,
                                'parent' => get_the_ID(),
                                'post_type' => 'publicacao_legal',
                            )
                        );
                        ?>
                    <ul class="list-group list-group-flush mb-0">
                        <?php
                            foreach ($children as $post) {
                                setup_postdata($post); ?>
                        <a class="list-group-item" href="<?php the_permalink() ?>">
                            <?php the_title(); ?> <i class="fas fa-long-arrow-alt-right fa"></i>
                        </a>
                        <?php } ?>
                    </ul>

                </div>
            </div>
        </div>
        <?php
    endwhile; // End of the loop.
    ?>
    </div>
</div>
<?php get_footer();